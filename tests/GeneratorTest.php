<?php


use Rodw\Generator\Generator;
use Symfony\Component\Filesystem\Filesystem;

class GeneratorTest extends PHPUnit_Framework_TestCase
{
    private $templateParser;

    protected function setUp()
    {
        parent::setUp();

        $this->templateParser = $this->getMockBuilder('Rodw\TemplateParser\TemplateParserInterface')
            ->setMethods(['template'])
            ->getMock();
    }

    public function test_to_generate_file_from_template()
    {
        $parsedTemplateValue = 'Parsed Template!!';

        $this->templateParser->expects($this->once())
            ->method('template')
            ->will($this->returnValue($parsedTemplateValue));

        // Path to save the file
        $savePath = __DIR__ . '/output/output.php';

        // Generate the file
        $generator = new Generator(new Filesystem(), $this->templateParser);
        $generator->generate($savePath, '', []);

        // The file content should be equal to the parsedTemplateValue
        $this->assertEquals($parsedTemplateValue, file_get_contents($savePath));
    }
}
