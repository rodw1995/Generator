<?php

namespace Rodw\Generator;


use Rodw\TemplateParser\TemplateParserInterface;
use Symfony\Component\Filesystem\Filesystem;

class Generator implements GeneratorInterface
{
    /**
     * @var Filesystem
     */
    private $filesystem;
    /**
     * @var TemplateParserInterface
     */
    private $template;

    /**
     * @param Filesystem $filesystem
     * @param TemplateParserInterface $template
     */
    public function __construct(Filesystem $filesystem, TemplateParserInterface $template)
    {
        $this->filesystem = $filesystem;
        $this->template = $template;
    }

    /**
     * Generate the file
     *
     * @param string $savePath
     * @param string $file
     * @param array $values
     */
    public function generate($savePath, $file, array $values)
    {
        $this->filesystem->dumpFile($savePath, $this->template->template($file, $values));
    }
}