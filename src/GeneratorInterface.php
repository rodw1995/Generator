<?php


namespace Rodw\Generator;


interface GeneratorInterface
{
    /**
     * Generate the file
     *
     * @param string $savePath
     * @param string $file
     * @param array $values | key value pair
     * @return
     */
    public function generate($savePath, $file, array $values);
}